/*
 * Phytium is pleased to support the open source community by making Phytium-optee available.
 * Copyright (c) 2023 Phytium Technology Co., Ltd.  All rights reserved.
 * Licensed under the BSD-2.0 License (the ""License""); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * https://opensource.org/license/bsd-license-php/
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an ""AS IS"" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <err.h>
#include <tee_client_api.h>

#define PTA_POOL_STATS_UUID { 0xD96A5B40, 0xE2C7, 0xB1AF, \
         { 0x87, 0x94, 0x10, 0x02, 0xA5, 0xD5, 0xC6, 0x1B } } 

#define TEE_ALLOCATOR_DESC_LENGTH 32
struct malloc_stats {
	char desc[TEE_ALLOCATOR_DESC_LENGTH];
	uint32_t allocated;               /* Bytes currently allocated */
	uint32_t max_allocated;           /* Tracks max value of allocated */
	uint32_t size;                    /* Total size for this allocator */
	uint32_t num_alloc_fail;          /* Number of failed alloc requests */
	uint32_t biggest_alloc_fail;      /* Size of biggest failed alloc */
	uint32_t biggest_alloc_fail_used; /* Alloc bytes when above occurred */
};

#define STATS_CMD_PAGER_STATS		0
#define STATS_CMD_ALLOC_STATS		1
#define STATS_CMD_MEMLEAK_STATS		2
#define STATS_CMD_TA_STATS		    3

int main(int argc, char *argv[])
{
    TEEC_Result res;
    TEEC_Context ctx;
    TEEC_Session sess;
    TEEC_Operation op;
    TEEC_UUID uuid = PTA_POOL_STATS_UUID;
    uint32_t err_origin;
	struct malloc_stats stats;

	res = TEEC_InitializeContext(NULL, &ctx);
	if (res != TEEC_SUCCESS)
		errx(1, "TEEC_InitializeContext failed with code 0x%x", res);

	res = TEEC_OpenSession(&ctx, &sess, &uuid,
						TEEC_LOGIN_PUBLIC, NULL, NULL, &err_origin);
	if (res != TEEC_SUCCESS)
		errx(1, "TEEC_Opensession failed with code 0x%x origin 0x%x",
						res, err_origin);

	memset(&op, 0, sizeof(op));
	op.paramTypes = TEEC_PARAM_TYPES(TEEC_VALUE_INPUT, TEEC_MEMREF_TEMP_OUTPUT, TEEC_NONE, TEEC_NONE);
    op.params[0].value.a = 1;
    op.params[0].value.b = 0;
    op.params[1].tmpref.buffer = &stats;
    op.params[1].tmpref.size = sizeof(struct malloc_stats);

    res = TEEC_InvokeCommand(&sess, STATS_CMD_ALLOC_STATS, &op, &err_origin);
    if(res != TEEC_SUCCESS)
        errx(1, "%s failed with code 0x%x origin 0x%x", argv[1], res, err_origin);

    printf("Name:%s\n", stats.desc);
    printf("Size:0x%x\n", stats.size);
    printf("A/M :0x%x/0x%x\n", stats.allocated, stats.max_allocated);

	memset(&op, 0, sizeof(op));
	op.paramTypes = TEEC_PARAM_TYPES(TEEC_VALUE_INPUT, TEEC_MEMREF_TEMP_OUTPUT, TEEC_NONE, TEEC_NONE);
    op.params[0].value.a = 3;
    op.params[0].value.b = 0;
    op.params[1].tmpref.buffer = &stats;
    op.params[1].tmpref.size = sizeof(struct malloc_stats);

    res = TEEC_InvokeCommand(&sess, STATS_CMD_ALLOC_STATS, &op, &err_origin);
    if(res != TEEC_SUCCESS)
        errx(1, "%s failed with code 0x%x origin 0x%x", argv[1], res, err_origin);

    printf("Name:%s\n", stats.desc);
    printf("Size:0x%x\n", stats.size);
    printf("A/M :0x%x/0x%x\n", stats.allocated, stats.max_allocated);

	TEEC_CloseSession(&sess);

	TEEC_FinalizeContext(&ctx);

    return 0;
}
