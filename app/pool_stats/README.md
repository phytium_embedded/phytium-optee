简介
----------

pool_stats:print TA_RAM memory pool stats

该app查询当前TA可以使用的内存池有多大，以及目前已经占用的大小。


输入参数说明
----------

1. pool_stats

        直接运行，不需要参数


反馈渠道
----------

如果对该app有什么疑问或建议，请mailto:dengqiang@phytium.com.cn

