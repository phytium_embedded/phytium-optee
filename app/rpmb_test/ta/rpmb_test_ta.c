/*
 * Phytium is pleased to support the open source community by making Phytium-optee available.
 * Copyright (c) 2023 Phytium Technology Co., Ltd.  All rights reserved.
 * Licensed under the BSD-2.0 License (the ""License""); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * https://opensource.org/license/bsd-license-php/
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an ""AS IS"" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <ta_rpmb_test.h>

TEE_Result TEE_EXEC_FAIL = 0x0000FFFF;
TEE_ObjectHandle g_FilesObj;

static TEE_Result rpmb_ta_create(uint32_t paramTypes, TEE_Param params[4])
{
    TEE_Result ret = TEE_EXEC_FAIL;
    void *object_id = NULL;
    uint32_t  obj_id_sz;
    TEE_ObjectHandle ref_handle = TEE_HANDLE_NULL;
    uint32_t obj_data_flag;
    uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_MEMREF_INPUT, TEE_PARAM_TYPE_NONE,
                                               TEE_PARAM_TYPE_NONE, TEE_PARAM_TYPE_NONE);

    if (exp_param_types != paramTypes) {
        return TEE_ERROR_BAD_PARAMETERS;
    }

    obj_data_flag = TEE_DATA_FLAG_ACCESS_READ |         /* we can later read the object */
                    TEE_DATA_FLAG_ACCESS_WRITE |        /* we can later write into the object */
                    TEE_DATA_FLAG_ACCESS_WRITE_META |   /* we can later destroy or rename the object */
                    TEE_DATA_FLAG_OVERWRITE;            /* destroy existing object of same ID */

    obj_id_sz = params[0].memref.size;
    object_id = TEE_Malloc(obj_id_sz, 0);
    if (!object_id) {
        return TEE_ERROR_OUT_OF_MEMORY;
    }

    TEE_MemMove(object_id, params[0].memref.buffer, obj_id_sz);

    ret = TEE_CreatePersistentObject(TEE_STORAGE_PRIVATE_RPMB, object_id, obj_id_sz, obj_data_flag, ref_handle,
                                     NULL, 0, &g_FilesObj);

    if (TEE_SUCCESS != ret) {
        EMSG("[CREATE] create file fail");
        return TEE_EXEC_FAIL;
    }
    else {
        TEE_CloseObject(g_FilesObj);
        return TEE_SUCCESS;
    }
}

static TEE_Result rpmb_ta_read(uint32_t paramTypes, TEE_Param params[4])
{
    TEE_ObjectHandle object;
    TEE_ObjectInfo object_info;
    TEE_Result ret;
    uint32_t read_bytes;
    char *obj_id;
    uint32_t obj_id_sz;
    char *data;
    uint32_t data_sz;
    uint32_t obj_data_flag;
    uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_MEMREF_INPUT, TEE_PARAM_TYPE_MEMREF_OUTPUT,
                                               TEE_PARAM_TYPE_NONE, TEE_PARAM_TYPE_NONE);

    if (exp_param_types != paramTypes) {
        return TEE_ERROR_BAD_PARAMETERS;
    }

    obj_data_flag = TEE_DATA_FLAG_ACCESS_READ | TEE_DATA_FLAG_SHARE_READ;
    obj_id_sz = params[0].memref.size;
    obj_id = TEE_Malloc(obj_id_sz, 0);
    if (!obj_id) {
        return TEE_ERROR_OUT_OF_MEMORY;
    }

    TEE_MemMove(obj_id, params[0].memref.buffer, obj_id_sz);

    data_sz = params[1].memref.size;
    data = TEE_Malloc(data_sz, 0);
    if (!data) {
        return TEE_ERROR_OUT_OF_MEMORY;
    }

    ret = TEE_OpenPersistentObject(TEE_STORAGE_PRIVATE_RPMB, obj_id, obj_id_sz, obj_data_flag, &object);

    if (ret != TEE_SUCCESS) {
        EMSG("Failed to open persistent object, res=0x%08x", ret);
        TEE_Free(obj_id);
        TEE_Free(data);
        return ret;
    }

    ret = TEE_GetObjectInfo1(object, &object_info);
    if (ret != TEE_SUCCESS) {
        EMSG("Failed to create persistent object, res=0x%08x", ret);
        goto exit;
    }

    if (object_info.dataSize > data_sz) {
        params[1].memref.size = object_info.dataSize;
        ret = TEE_ERROR_SHORT_BUFFER;
        goto exit;
    }

    ret = TEE_ReadObjectData(object, data, object_info.dataSize, &read_bytes);
    if (ret == TEE_SUCCESS) {
        TEE_MemMove(params[1].memref.buffer, data, read_bytes);
    }
    if (ret != TEE_SUCCESS || read_bytes != object_info.dataSize) {
        EMSG("TEE_ReadObjectData failed 0x%08x, read %" PRIu32 " over %u", ret, read_bytes, object_info.dataSize);
        goto exit;
    }

    params[1].memref.size = read_bytes;

exit:
    TEE_CloseObject(object);
    TEE_Free(obj_id);
    TEE_Free(data);
    return ret;
}

static TEE_Result rpmb_ta_write(uint32_t paramTypes, TEE_Param params[4])
{
    TEE_Result ret;
    char *obj_id;
    size_t obj_id_sz;
    char *data;
    size_t data_sz;
    uint32_t obj_data_flag;
    uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_MEMREF_INPUT, TEE_PARAM_TYPE_MEMREF_INPUT,
                                               TEE_PARAM_TYPE_NONE, TEE_PARAM_TYPE_NONE);

    if (exp_param_types != paramTypes) {
        return TEE_ERROR_BAD_PARAMETERS;
    }

    obj_id_sz = params[0].memref.size;
    obj_id = TEE_Malloc(obj_id_sz, 0);
    if (!obj_id) {
        return TEE_ERROR_OUT_OF_MEMORY;
    }
    TEE_MemMove(obj_id, params[0].memref.buffer, obj_id_sz);
   
    data_sz = params[1].memref.size;
    data = TEE_Malloc(data_sz, 0);
    if (!data) {
        return TEE_ERROR_OUT_OF_MEMORY;
    }
    TEE_MemMove(data, params[1].memref.buffer, data_sz);

    obj_data_flag = TEE_DATA_FLAG_ACCESS_WRITE | TEE_DATA_FLAG_SHARE_WRITE;

    ret = TEE_OpenPersistentObject(TEE_STORAGE_PRIVATE_RPMB, obj_id, obj_id_sz, obj_data_flag, &g_FilesObj);
    ret = TEE_WriteObjectData(g_FilesObj, data, data_sz);

    if (ret != TEE_SUCCESS) {
        TEE_CloseAndDeletePersistentObject1(g_FilesObj);
    } else {
        TEE_CloseObject(g_FilesObj);
    }

    TEE_Free(obj_id);
    TEE_Free(data);

    if (TEE_SUCCESS != ret) {
        return TEE_EXEC_FAIL;
    }
    else {
        return TEE_SUCCESS;
    }

    return ret;
}

static TEE_Result rpmb_ta_rename(uint32_t paramTypes, TEE_Param params[4])
{
    TEE_Result ret = TEE_SUCCESS;
    char *obj_id;
    uint32_t obj_id_sz;
    char *object_id;
    uint32_t obj_data_flag;
    uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_MEMREF_INPUT, TEE_PARAM_TYPE_MEMREF_INPUT,
                                               TEE_PARAM_TYPE_NONE, TEE_PARAM_TYPE_NONE);

    if (exp_param_types != paramTypes) {
        return TEE_ERROR_BAD_PARAMETERS;
    }

    obj_data_flag = TEE_DATA_FLAG_ACCESS_WRITE | TEE_DATA_FLAG_SHARE_WRITE | TEE_DATA_FLAG_ACCESS_WRITE_META;
    obj_id_sz = params[0].memref.size;
    obj_id = TEE_Malloc(obj_id_sz, 0);
    if (!obj_id)
        return TEE_ERROR_OUT_OF_MEMORY;

    TEE_MemMove(obj_id, params[0].memref.buffer, obj_id_sz);

    ret = TEE_OpenPersistentObject(TEE_STORAGE_PRIVATE_RPMB, obj_id, obj_id_sz, obj_data_flag, &g_FilesObj);

    object_id = TEE_Malloc(params[1].memref.size, 0);
    if (!object_id) {
        return TEE_ERROR_OUT_OF_MEMORY;
    }

    TEE_MemMove(object_id, params[1].memref.buffer, params[1].memref.size);

    ret = TEE_RenamePersistentObject(g_FilesObj, object_id, params[1].memref.size);

    TEE_CloseObject(g_FilesObj);
    
    if (TEE_SUCCESS != ret) {
        return TEE_EXEC_FAIL;
    }
    else {
        return TEE_SUCCESS;
    }
}

static TEE_Result rpmb_ta_delete(uint32_t paramTypes, TEE_Param params[4])
{
    TEE_Result ret = TEE_SUCCESS;
    char *obj_id;
    uint32_t obj_id_sz;
    uint32_t obj_data_flag;
    uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_MEMREF_INPUT, TEE_PARAM_TYPE_NONE,
                                               TEE_PARAM_TYPE_NONE, TEE_PARAM_TYPE_NONE);

    if (exp_param_types != paramTypes) {
        return TEE_ERROR_BAD_PARAMETERS;
    }

    obj_data_flag = TEE_DATA_FLAG_ACCESS_WRITE | TEE_DATA_FLAG_SHARE_WRITE | TEE_DATA_FLAG_ACCESS_WRITE_META;
    obj_id_sz = params[0].memref.size;
    obj_id = TEE_Malloc(obj_id_sz, 0);
    if (!obj_id) {
        return TEE_ERROR_OUT_OF_MEMORY;
    }

    TEE_MemMove(obj_id, params[0].memref.buffer, obj_id_sz);
    ret = TEE_OpenPersistentObject(TEE_STORAGE_PRIVATE_RPMB, obj_id, obj_id_sz, obj_data_flag, &g_FilesObj);

    if (TEE_SUCCESS != ret) {
        return TEE_EXEC_FAIL;
    }

    TEE_CloseAndDeletePersistentObject(g_FilesObj);

    if (TEE_SUCCESS != ret) {
        return TEE_EXEC_FAIL;
    }
    else {
        return TEE_SUCCESS;
    }
}

TEE_Result TA_CreateEntryPoint(void)
{
    return TEE_SUCCESS;
}

void TA_DestroyEntryPoint(void)
{
}

TEE_Result TA_OpenSessionEntryPoint(uint32_t param_types,
        TEE_Param  params[4], void **sess_ctx)
{
    uint32_t exp_param_types = TEE_PARAM_TYPES(TEE_PARAM_TYPE_NONE, TEE_PARAM_TYPE_NONE,
                                               TEE_PARAM_TYPE_NONE, TEE_PARAM_TYPE_NONE);
    if (exp_param_types != param_types) {
        return TEE_ERROR_BAD_PARAMETERS;
    }

    /* Unused parameters */
    (void)&params;
    (void)&sess_ctx;

    return TEE_SUCCESS;
}

void TA_CloseSessionEntryPoint(void *sess_ctx)
{
    (void)&sess_ctx; /* Unused parameter */
}

TEE_Result TA_InvokeCommandEntryPoint(void *sess_ctx, uint32_t cmd_id,
            uint32_t param_types, TEE_Param params[4])
{
    TEE_Result ret = TEE_SUCCESS;
    (void)&sess_ctx;

    switch(cmd_id)
    {
        case CMD_RPMB_TEST_CREATE:
            ret = rpmb_ta_create(param_types, params);
            break;

        case CMD_RPMB_TEST_READ:
            ret = rpmb_ta_read(param_types, params);
            break;

        case CMD_RPMB_TEST_WRITE:
            ret = rpmb_ta_write(param_types, params);
            break;

        case CMD_RPMB_TEST_RENAME:
            ret = rpmb_ta_rename(param_types, params);
            break;

        case CMD_RPMB_TEST_DELETE:
            ret = rpmb_ta_delete(param_types, params);
            break;   

        default:
            ret = TEE_ERROR_BAD_PARAMETERS;
            break;   
    }

    return  ret;
}

