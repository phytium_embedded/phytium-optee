/*
 * Phytium is pleased to support the open source community by making Phytium-optee available.
 * Copyright (c) 2023 Phytium Technology Co., Ltd.  All rights reserved.
 * Licensed under the BSD-2.0 License (the ""License""); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * https://opensource.org/license/bsd-license-php/
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an ""AS IS"" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef TA_RPMB_TEST_H
#define TA_RPMB_TEST_H

#include <string.h>
#include <trace.h>
#include <tee_internal_api.h>
#include <tee_api_defines.h>
#include <tee_internal_api_extensions.h>

#define TA_RPMB_TEST_UUID { 0x89caff9d, 0x525e, 0x30e8, \
        { 0x97,0x22,0xfd,0x1a,0x2b,0x28,0x58,0xc6 } }

enum rpmb_test_cmd_type {
    CMD_RPMB_TEST_CREATE,
    CMD_RPMB_TEST_WRITE,
    CMD_RPMB_TEST_READ,
    CMD_RPMB_TEST_RENAME,
    CMD_RPMB_TEST_DELETE,
};

#define   FAIL     -1
#define   OK        0

#endif /*TA_RPMB_TEST_H*/
