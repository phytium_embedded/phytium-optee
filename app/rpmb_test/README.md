简介
----------

rpmb_test:Test EMMC's RPMB partition

该app完成RPMB安全分区的基本操作测试，可以用于RPMB分区读写的验证。


输入参数说明
----------

1. rpmb_test

        直接运行，不需要参数


源码修改说明
----------

该app测试运行需要修改OPTEE多项默认配置。所以需要根据实际情况对源码作出一些修改。

1. $(call force,CFG_RPMB_FS,y)

        需要将安全存储模式修改为RPMB_FS方式，系统默认是REE_FS方式，两者是互斥的。
        建议将该项配置加入BSP对应的board.mk中，不要直接修改系统的config.mk。

2. $(call force,CFG_RPMB_TESTKEY,y)

        RPMB分区操作需要RPMB Key验证身份，系统提供一个固定的Test Key，正常应该从硬件HUK派生出来。
        普通测试下建议使用该Test Key，即将该项配置加入BSP对应的board.mk中。

3. $(call force,CFG_RPMB_WRITE_KEY,y)

        该项配置加入BSP对应的board.mk中，表示系统使用RPMB Key写入驱动。
        该驱动的作用是当系统确认当前RPMB分区的RPMB Key是不存在时，会自动调用驱动将准备好的Key写入。
        特别注意：RPMB Key一旦写入真实的RPMB分区后，是永远无法修改和更换的。

4. $(call force,CFG_REE_FS_INTEGRITY_RPMB,n)

        当RPMB_EMU配置被修改成0时，该项配置必须加入BSP对应的board.mk中，即关闭REE_FS_INTEGRITY_RPMB功能。
        因为该功能会导致系统加载TA程序是出现冲突错误。
        造成冲突的原因是因为系统处于安全性考虑，会在加载TA程序的时候去验证相关的hash值，以防加载环境被非法篡改。
        当RPMB有效时，历史的hash值会存放在RPMB中，但测试的REE侧环境肯定在变化，所以会产生冲突。
        当RPMB_EMU配置是1时，使用模拟的RPMB分区，每次启动都会清空历史的hash值，所以系统会新建数据，并允许本次检查通过。

5. RPMB_EMU

        该配置在optee_client/tee-supplicant/Makefile中，是Client端的配置。
        默认RPMB_EMU=1,表示当前是使用的模拟RPMB分区来响应TEE侧请求，修改为0时，则表示是使用真实的RPMB分区。
        特别注意：建议不熟悉时，先使用模拟RPMB分区。一旦使用真实的分区，有些操作是不可逆的，比如RPMB Key写入。

6. app联编

        修改app/build_app脚本，在module_name字符串常量中增加rpmb_test项，可以参考其它app的写法，完成和其他软件联编。


反馈渠道
----------

如果对该app有什么疑问或建议，请mailto:dengqiang@phytium.com.cn

