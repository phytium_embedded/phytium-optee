/*
 * Phytium is pleased to support the open source community by making Phytium-optee available.
 * Copyright (c) 2023 Phytium Technology Co., Ltd.  All rights reserved.
 * Licensed under the BSD-2.0 License (the ""License""); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * https://opensource.org/license/bsd-license-php/
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an ""AS IS"" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <string.h>
#include <tee_client_api.h>
#include <ta_rpmb_test.h>

char file0[] = "TestFile0.txt";
char file1[] = "ChangeTestFile1.txt";

char read_buf[64] = {0};
char file_text[] = "Test file text for rpmb test!";

TEEC_UUID svc_id = TA_RPMB_TEST_UUID;
TEEC_Context rpmb_test_context;

static TEEC_Result rpmb_test_create(TEEC_Session* session, char* file_name, uint32_t file_len)
{
    TEEC_Operation op;
    TEEC_Result result;
    uint32_t origin;

    memset(&op, 0x0, sizeof(TEEC_Operation));
    op.paramTypes = TEEC_PARAM_TYPES(TEEC_MEMREF_TEMP_INPUT, TEEC_NONE, TEEC_NONE, TEEC_NONE);
    op.params[0].tmpref.buffer = file_name;
    op.params[0].tmpref.size = file_len;

    result = TEEC_InvokeCommand(session, CMD_RPMB_TEST_CREATE, &op, &origin);
    if (result != TEEC_SUCCESS) {
        printf("[%s]InvokeCommand failed, ReturnCode=0x%x, ReturnOrigin=0x%x\n", __FUNCTION__, result, origin);
    }

    return result;
}

static TEEC_Result rpmb_test_write(TEEC_Session* session, char* file_name, uint32_t file_len, uint32_t in_len, char* input)
{
    TEEC_Operation op;
    TEEC_Result result;
    uint32_t origin;

    memset(&op, 0x0, sizeof(TEEC_Operation));
    op.started = 1;
    op.paramTypes = TEEC_PARAM_TYPES(TEEC_MEMREF_TEMP_INPUT, TEEC_MEMREF_TEMP_INPUT, TEEC_NONE, TEEC_NONE);
    op.params[0].tmpref.buffer = file_name;
    op.params[0].tmpref.size = file_len;
    op.params[1].tmpref.buffer = input;
    op.params[1].tmpref.size = in_len;

    result = TEEC_InvokeCommand(session, CMD_RPMB_TEST_WRITE, &op, &origin);
    if (result != TEEC_SUCCESS) {
        printf("[%s]InvokeCommand failed, ReturnCode=0x%x, ReturnOrigin=0x%x\n", __FUNCTION__, result, origin);
    }

    return result;
}

static TEEC_Result rpmb_test_read(TEEC_Session* session, char* file_name, uint32_t file_len, uint32_t out_len, char* output)
{
    TEEC_Operation op;
    TEEC_Result result;
    uint32_t origin;

    memset(&op, 0x0, sizeof(TEEC_Operation));
    op.started = 1;
    op.paramTypes = TEEC_PARAM_TYPES(TEEC_MEMREF_TEMP_INPUT, TEEC_MEMREF_TEMP_OUTPUT, TEEC_NONE, TEEC_NONE);
    op.params[0].tmpref.buffer = file_name;
    op.params[0].tmpref.size = file_len;
    op.params[1].tmpref.buffer = output;
    op.params[1].tmpref.size = out_len;

    result = TEEC_InvokeCommand(session, CMD_RPMB_TEST_READ, &op, &origin);
    if (result != TEEC_SUCCESS) {
        printf("[%s]InvokeCommand failed, ReturnCode=0x%x, ReturnOrigin=0x%x\n", __FUNCTION__, result, origin);
    }

    return result;
}

static TEEC_Result rpmb_test_rename(TEEC_Session* session, char* old_name, uint32_t old_len, char* new_name, uint32_t new_len)
{
    TEEC_Operation op;
    TEEC_Result result;
    uint32_t origin;

    memset(&op, 0x0, sizeof(TEEC_Operation));
    op.started = 1;
    op.paramTypes = TEEC_PARAM_TYPES(TEEC_MEMREF_TEMP_INPUT, TEEC_MEMREF_TEMP_INPUT, TEEC_NONE, TEEC_NONE);
    op.params[0].tmpref.buffer = old_name;
    op.params[0].tmpref.size = old_len;
    op.params[1].tmpref.buffer = new_name;
    op.params[1].tmpref.size = new_len;

    result = TEEC_InvokeCommand(session, CMD_RPMB_TEST_RENAME, &op, &origin);
    if (result != TEEC_SUCCESS) {
        printf("[%s]InvokeCommand failed, ReturnCode=0x%x, ReturnOrigin=0x%x\n", __FUNCTION__, result, origin);
    }

    return result;
}

static TEEC_Result rpmb_test_delete(TEEC_Session* session, char* file_name, uint32_t file_len)
{
    TEEC_Operation op;
    TEEC_Result result;
    uint32_t origin;

    memset(&op, 0x0, sizeof(TEEC_Operation));
    op.started = 1;
    op.paramTypes = TEEC_PARAM_TYPES(TEEC_MEMREF_TEMP_INPUT, TEEC_NONE, TEEC_NONE, TEEC_NONE);
    op.params[0].tmpref.buffer = file_name;
    op.params[0].tmpref.size = file_len;

    result = TEEC_InvokeCommand(session, CMD_RPMB_TEST_DELETE, &op, &origin);
    if (result != TEEC_SUCCESS) {
        printf("[%s]InvokeCommand failed, ReturnCode=0x%x, ReturnOrigin=0x%x\n", __FUNCTION__, result, origin);
    }

    return result;
}

int main(int argc, char *argv[])
{
    TEEC_Session    session;
    TEEC_Result     result;
    uint32_t        origin;

    printf("RPMB Test file0:%s\n", file0);
    printf("          file1:%s\n", file1);
    printf("Test text      :%s\n\n", file_text);

    result = TEEC_InitializeContext(NULL, &rpmb_test_context);
    if (result != TEEC_SUCCESS) {
        printf("InitializeContext failed, ReturnCode=0x%x\n", result);
        goto main_return;
    }

    result = TEEC_OpenSession(&rpmb_test_context, &session, &svc_id, 
                                TEEC_LOGIN_PUBLIC, NULL, NULL, &origin);
    if (result != TEEC_SUCCESS) {
        printf("OpenSession failed, ReturnCode=0x%x, ReturnOrigin=0x%x\n", result, origin);
        goto main_return;
    } 

    printf("Step 1  :Create file0\n");
    result = rpmb_test_create(&session, file0, sizeof(file0));
    if (result != TEEC_SUCCESS) {
        printf("        :Create file0 failed\n");
        goto main_return;
    }
    else {
        printf("        :Create file0 OK\n");
    }

    printf("Step 2  :Write text to file0\n");
    result = rpmb_test_write(&session, file0, sizeof(file0), sizeof(file_text), file_text);
    if (result != TEEC_SUCCESS) {
        printf("        :Write text to file0 failed\n");
        goto main_return;
    }
    else {
        printf("        :Write text to file0 OK\n");
    }

    printf("Step 2-1:Read text from file0 for verification\n");
    memset(read_buf, 0, 64);
    result = rpmb_test_read(&session, file0, sizeof(file0), 60, read_buf);
    if (result != TEEC_SUCCESS) {
        printf("        :Read text from file0 failed\n");
    }
    else {
        printf("        :Read text from file0 OK\n");
        printf("Text    :%s\n", read_buf);
    }

    printf("Step 3  :Rename file0 to file1\n");
    result = rpmb_test_rename(&session, file0, sizeof(file0), file1, sizeof(file1));
    if (result != TEEC_SUCCESS) {
        printf("        :Rename file0 to file1 failed\n");
        goto main_return;
    }
    else {
        printf("        :Rename file0 to file1 OK\n");
    }

    printf("Step 3-1:Read file0 must be failed\n");
    memset(read_buf, 0, 64);
    result = rpmb_test_read(&session, file0, sizeof(file0), 60, read_buf);
    if (result != TEEC_SUCCESS) {
        printf("        :Read text from file0 failed\n");
    }
    else {
        printf("        :Read text from file0 OK\n");
        printf("Text    :%s\n", read_buf);
    }
    
    printf("Step 3-2:Read file1 must be OK\n");
    memset(read_buf, 0, 64);
    result = rpmb_test_read(&session, file1, sizeof(file1), 60, read_buf);
    if (result != TEEC_SUCCESS) {
        printf("        :Read text from file1 failed\n");
    }
    else {
        printf("        :Read text from file1 OK\n");
        printf("Text    :%s\n", read_buf);
    }

    printf("Step 4  :Delete file1\n");
    result = rpmb_test_delete(&session, file1, sizeof(file1));
    if (result != TEEC_SUCCESS) {
        printf("        :Delete file1 failed\n");
        goto main_return;
    }
    else {
        printf("        :Delete file1 OK\n");
    }

    printf("Step 4-1:Read file1 must be failed\n");
    memset(read_buf, 0, 64);
    result = rpmb_test_read(&session, file1, sizeof(file1), 60, read_buf);
    if (result != TEEC_SUCCESS) {
        printf("        :Read text from file1 failed\n");
    }
    else {
        printf("        :Read text from file1 OK\n");
        printf("Text    :%s\n", read_buf);
    }

    printf("RPMB Test over\n");

main_return:
    TEEC_CloseSession(&session);
    TEEC_FinalizeContext(&rpmb_test_context);
    return 0;
}
