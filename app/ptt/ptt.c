/*
 * Phytium is pleased to support the open source community by making Phytium-optee available.
 * Copyright (c) 2023 Phytium Technology Co., Ltd.  All rights reserved.
 * Licensed under the BSD-2.0 License (the ""License""); you may not use this file except in
 * compliance with the License. You may obtain a copy of the License at
 * https://opensource.org/license/bsd-license-php/
 * Unless required by applicable law or agreed to in writing, software distributed under the License is
 * distributed on an ""AS IS"" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <err.h>
#include <tee_client_api.h>

#define PTA_PTT_UUID { 0x82DA116D, 0xC09C, 0x737E, \
         { 0xF1, 0x90, 0x99, 0xE1, 0x03, 0x87, 0x31, 0xD5} } 

#define PTT_BUF_MAX         256

int main(int argc, char *argv[])
{
    TEEC_Result res;
    TEEC_Context ctx;
    TEEC_Session sess;
    TEEC_Operation op;
    TEEC_UUID uuid = PTA_PTT_UUID;
    uint32_t err_origin;
    char cBuf[PTT_BUF_MAX] = {0};

    if(argc < 2)
    {
        printf("ptt help:\n");
        printf("ptt 100: print PBF version\n");
        printf("ptt 200: print base TEE OS version\n");
        printf("ptt 201: print TEE OS compiled time\n");
        printf("ptt 202: print Phytium TEE OS version\n");
    }
    else
    {
        res = TEEC_InitializeContext(NULL, &ctx);
        if (res != TEEC_SUCCESS)
            errx(1, "TEEC_InitializeContext failed with code 0x%x", res);

        res = TEEC_OpenSession(&ctx, &sess, &uuid,
                            TEEC_LOGIN_PUBLIC, NULL, NULL, &err_origin);
        if (res != TEEC_SUCCESS)
            errx(1, "TEEC_Opensession failed with code 0x%x origin 0x%x",
                            res, err_origin);

        memset(&op, 0, sizeof(op));
        op.paramTypes = TEEC_PARAM_TYPES(TEEC_MEMREF_TEMP_OUTPUT, TEEC_NONE, TEEC_NONE, TEEC_NONE);
        op.params[0].tmpref.buffer = cBuf;
        op.params[0].tmpref.size = PTT_BUF_MAX;

        res = TEEC_InvokeCommand(&sess, atoi(argv[1]), &op, &err_origin);
        if(res != TEEC_SUCCESS)
            errx(1, "%s failed with code 0x%x origin 0x%x", argv[1], res, err_origin);

        printf("%s\n", cBuf);

        TEEC_CloseSession(&sess);

        TEEC_FinalizeContext(&ctx);
    }
    return 0;
}
