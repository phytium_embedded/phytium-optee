简介
----------

ptt:Phytium TEE Test

该app使用不同输入参数，完成TEE侧一些简单的测试项目。


输入参数说明
----------

1. ptt 100

        查询PBF固件版本

2. ptt 200

        查询OPTEE版本

3. ptt 201

        查询OPTEE编译时间

4. ptt 202

        查询OPTEE飞腾内部版本号


反馈渠道
----------

如果对该app有什么疑问或建议，请mailto:dengqiang@phytium.com.cn

