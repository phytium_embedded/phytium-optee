# patches目录说明
# 1 简介
<pre>
   本目录主要存放某单一功能的补丁，考虑到功能并没有通用性，所以并没有合入主线，只是以patch的形式存在
   如果条件符合的硬件环境，可以考虑打入该patch，也可以作为参考
</pre>
<br/>
<br/>

# 2 建议操作方法
### 2.1 合入patch
```bash
   git apply patches/xxx.patch
```
### 2.2 取消合入
```bash
   git apply patches/xxx.patch -R
```
<br/>
<br/>

# 3 环境部署
<pre>
   由于patch的合入，最终编译结果可能和主线结果有一定差别，会对目标环境有一定影响，建议对REE侧环境进行升级更新
   先在REE侧环境启动后，解压编译out目录中新的REE侧支持包xxx.tgz到工作目录下
   然后工作目录中会出现一个data目录，内容和编译out目录中的data目录保持一致
   再将该data目录中bin、lib、optee_armtz目录内容copy到REE侧运行环境中的对应目录，然后重启REE侧系统即可
   如果有拷贝的权限问题，请自行解决
</pre>
```bash
   cp -dpf data/bin/* /usr/bin
   cp -dpf data/lib/* /usr/lib
   cp data/optee_armtz/* /data/optee_armtz
```
<pre>
   如果环境是运行env.source手动建立的，那就直接运行env.source即可
</pre>
<br/>
<br/>

# 4 具体patch说明
### 4.1 e2000qdemo_rpmb_test
<pre>
   该patch只针对有eMMC RPMB分区的硬件环境，比如E2000 demo版，飞腾派的eMMC版本
   该patch将使能项目app目录中的rpmb_test测试用例
   该patch默认是使用optee源码中的test key作为rpmb key
   该patch在e2000q demo板环境测试OK，测试对应项目tag是v4.5.1
   特别注意：
   按照RPMB分区的硬件特性，rpmb key存储在OTP中，只能写入一次，永远不能修改，实际产品应该引入HUK等信息派生出key
</pre>
<br/>
<br/>
