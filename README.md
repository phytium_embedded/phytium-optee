# Phytium-optee项目
# 1 简介
<pre>
   本项目主要是为支持飞腾嵌入式处理器验证TEE运行环境，包含optee_os、optee_client、app目录
   软件编译生成TEE OS系统*bin程序镜像文件及REE侧数据文件，包含相关测试验证用例
</pre>
<br/>
<br/>

# 2 文档支持
<pre>
   和项目有关的一些用户手册、技术分享，提交在飞腾嵌入式软件部的doc总仓库，具体
   请阅读https://gitee.com/phytium_embedded/phytium-embedded-docs/tree/master/optee 目录下对应详细说明文件
</pre>
<br/>
<br/>

# 3 单独编译
### 3.1 获取项目代码
```bash
   git clone git@gitee.com:phytium_embedded/phytium-optee.git
```
<pre>
   默认是获取master上最新代码，但项目也按标签来管理的，可以指定需要的标签来clone
</pre>
<br/>

### 3.2 配置编译环境
```bash
   export PATH=$PATH:[your toolchain path]/bin
   export ARCH=arm64 CROSS_COMPILE64=[cross compiler prefix]
```
<pre>
   将编译工具指定为项目默认编译工具，GCC版本尽量在7.2以上，根据实际需要选择32bit还是64bit
</pre>
<br/>

### 3.3 一键编译
```bash
   cd xxx/phytium-optee
   ./build_all e2000qdemo d
```
<pre>
   第一次使用，可以先单独执行build_all脚本，根据help提示了解输入参数的含义
   编译成功后，在目录xxx/phytium-optee/out下，会生成TEE OS bin程序镜像文件、REE侧数据文件及已经打包的tgz文件
</pre>
<br/>
<br/>

# 4 标签（tag）说明
   项目在v4.5.0之前，标签和OP-TEE官方版本的对应关系如下表：
|项目标签|基于的OP-TEE版本|
|:-------:|:-------:|
|v3.2|v4.2.0|
|v3.0~v3.1|v4.0.0|
|v2.0~v2.3|v3.20.0|
|v1.0~v1.2|v3.12.0|
|||
<pre>
   项目在v4.5.0之后，标签的前两段将严格和基于的官方版本保持一致
   新增加的小版本号为0时，表示基于上一个大版本的迁移修改
   新增加的小版本号不为0时，表示对0版本的第几次升级修改
   新增加的小版本号以15天为限制提交周期，即小版本升级后的15天之内不会再提交新的小版本，直到有价值的修改出现
   当小版本的修改内容为比较重要的代码时，会在对应的硬件环境中重新测试一遍
   运行环境下，可以通过ptt小工具查询当前版本标签值，例如：
</pre>
```bash
   >ptt 202
   >Phytium-optee verison : v4.5.0
```
<br/>
<br/>

# 5 验证
<pre>
   限于硬件环境的配置，目前项目的功能验证只限于E2000Q demo板环境，其他支持硬件板级环境出现问题欢迎通过正常途径进行反馈。
</pre>
<br/>
<br/>

# 6 深入支持和bug反馈
<pre>
   飞腾嵌入式处理器遵循飞腾PSPA安全规范，其安全特性可以为嵌入式可信执行环境提供支持，飞腾亦以OP-TEE为基础进行了安全强化及特性拓展研究。
如需更深入的技术支持和交流探讨，欢迎垂询！
   本项目如果有问题或bug需要反馈，可以通过Gitee提交issue，也欢迎直接联系相关人员的邮箱：dengqiang@phytium.com.cn
</pre>
<br/>
<br/>
